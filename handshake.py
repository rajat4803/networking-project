import sys
import socket
import random

#peer = {'peers': ['199.241.184.162:6881', '91.189.95.21:6906', '91.189.95.21:6919', '185.56.20.44:58893', '185.123.15.207:59723'], 'interval': 1800, 'complete': 13, 'incomplete': 0}
class HandShake(object):
	def __init__(self,peerlist,hashr,params):
		self.peer = peerlist.keys()
		self.ip_port = self.peer['peers']
		self.ipportlist = [tuple(x.split(':')) for x in self.ip_port]
		self.hashr = hashr
		self.params = params


	def generate_handshake(info_hash, peer_id):
    	protocol_id = "BitTorrent protocol"
		len_id = str(len(protocol_id))
		print(len_id)
		reserved = "00000000"
		return len_id + protocol_id + reserved + info_hash + peer_id


	def send_recv_handshake(handshake, host, port):	
	    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, port))
		s.send(handshake)
		data = s.recv(len(handshake))
		s.close()
		return data

	def handshake():
		for x, y in self.ipportlist:
			h = generate_handshake(self.hashr,self.params['peer_id'])
			print(len(h))
			b = self.send_recv_handshake(h,x,int(y))
			print(b)
