__author__ = 'Ayush_Rajat_Suman'

import time
import Peer
from threading import Thread
from pubsub import pub


class FuncThread(Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        Thread.__init__(self)

    def run(self):
        self._target(*self._args)
        

class PeerSeeker(Thread):
    def __init__(self, newpeersQueue, torrent):
        Thread.__init__(self)
        self.newpeersQueue = newpeersQueue
        self.torrent = torrent
        self.peerFailed = [("","")]
        self.lstThreads=[]

    def threadcheck(self,peer):
        if not (peer[0],peer[1]) in self.peerFailed:
                p = Peer.Peer(self.torrent,peer[0],peer[1])
        if not p.connectToPeer(20):
            self.peerFailed.append((peer[0],peer[1]))
        else:
            pub.sendMessage('PeersManager.newPeer',peer=p)
            
    def run(self):
        while True:
            peer = self.newpeersQueue.get()
            t1 = FuncThread(self.threadcheck,peer)
            self.lstThreads.append(t1)
            t1.start()        
            
