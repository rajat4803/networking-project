__author__ = 'Ayush_Rajat_Suman'

import bencode
import requests
import logging
import struct,random,socket
from urlparse import urlparse
import threading,time

class FuncThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)

    def run(self):
        self._target(*self._args)
        
class Tracker(object):
    def __init__(self, torrent, newpeersQueue,proxies={},port=8090):
        self.torrent = torrent
        self.lstThreads =  []
        self.newpeersQueue = newpeersQueue
        self.proxies=proxies
        self.port=port
        self.getPeersFromTrackers()


    def getPeersFromTrackers(self):
        for tracker in self.torrent.announceList:
            if tracker[0][:4] == "http":
                t1 = FuncThread(self.scrapeHTTP,self.torrent,tracker[0])
                self.lstThreads.append(t1)
                t1.start()
            else:
                t2 = FuncThread(self.scrape_udp,self.torrent, tracker[0])
                self.lstThreads.append(t2)
                t2.start()

        for t in self.lstThreads:
            t.join()
            
            
    def parseTrackerResponse(self,buf):
        offset=0
        while offset != len(buf):
            alpha = socket.inet_ntoa(buf[offset:offset+4])
            offset += 4
            if offset >= len(buf):
                raise RuntimeError("Error while reading peer port")
            beta = struct.unpack_from("!H",buf,offset)[0]
            offset += 2
            self.newpeersQueue.put([alpha, beta])
    
    def parsetrackerresp(self,p):
        offset=1
        info = {}
        while offset+1<len(p):
             value0, offset = bencode.decode_func[p[offset]](p, offset)
             value1, offset = bencode.decode_func[p[offset]](p, offset)
             info[value0]=value1     
        info['peers'] = self.parseTrackerResponse(info['peers'])
        return(info)
        
    def make_connection_id_request(self):
        conn_id = struct.pack('>Q', 0x41727101980)
        action = struct.pack('>I', 0)
        trans_id = struct.pack('>I', random.randint(0, 100000))

        return (conn_id + action + trans_id, trans_id, action)

    def make_announce_input(self,info_hash, conn_id, peer_id):
        action = struct.pack('>I', 1)
        trans_id = struct.pack('>I', random.randint(0, 100000))

        downloaded = struct.pack('>Q', 0)
        left = struct.pack('>Q', 0)
        uploaded = struct.pack('>Q', 0)

        event = struct.pack('>I', 0)
        ip = struct.pack('>I', 0)
        key = struct.pack('>I', 0)
        num_want = struct.pack('>i', -1)
        port = struct.pack('>h', 8000)

        msg = (conn_id + action + trans_id + info_hash + peer_id + downloaded +
                left + uploaded + event + ip + key + num_want + port)

        return msg, trans_id, action
        
    def send_msg(self,conn, sock, msg, trans_id, action, size,announce):
        sock.sendto(msg, conn)
        try:
            response = sock.recv(2048)
        except socket.timeout:
            print "Socket Timeout",announce,"\n"
        if len(response) < size:
            return self.send_msg(conn, sock, msg, trans_id, action, size)

        if action != response[0:4] or trans_id != response[4:8]:
            return self.send_msg(conn, sock, msg, trans_id, action, size)

        return response
        
    
    def scrape_udp(self,torrent, announce):
        try:
            parsed = urlparse(announce)
            ip = socket.gethostbyname(parsed.hostname)

            if ip == '127.0.0.1':
                return False
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.settimeout(8)
            conn = (ip, parsed.port)
            msg, trans_id, action = self.make_connection_id_request()
            response = self.send_msg(conn, sock, msg, trans_id, action, 16,announce)
            if response == None:
                return ""

            conn_id = response[8:]
            msg, trans_id, action = self.make_announce_input(torrent.info_hash, conn_id, torrent.peer_id)
            response = self.send_msg(conn, sock, msg, trans_id, action, 20,announce)
            if response == None or response == "":
                return ""
            self.parseTrackerResponse(response[20:])
        except:
            pass
                      
    def scrapeHTTP(self,torrent,tracker):
        hashdigest = torrent.info_hash
        params = {'info_hash' : hashdigest,
          'peer_id' : self.torrent.peer_id,
          'uploaded' : '0',
          'downloaded' : '0',
          'port' : self.port,
          'left' : torrent.totalLength,
          'compact': 1
          }
        info = requests.get(tracker,params,proxies=self.proxies)._content
        self.parsetrackerresp(info)
            
