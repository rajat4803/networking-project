# coding=utf-8
import bencode
import random
import requests
import struct,socket


def generate_handshake(info_hash, peer_id):
    """ Returns a handshake. """
    myshake = {
    'protocol_id' : "BitTorrent protocol",
    'len_id' : '\x13',
    'reserved' : "00000000",
    'info_hash' : info_hash,
    'peer_id'  : peer_id }
    return (myshake['len_id'] + myshake['protocol_id'] + myshake['reserved'] + myshake['info_hash'] + myshake['peer_id']),myshake


def parsehandshake(buf,myshake):
    len_id='\x13'
    offset=0
    print(repr(buf))
    while offset != len(buf):
        #len_id
        len_id = struct.unpack_from("1s",buf,offset)[0]
        print(repr(len_id))
        if(len_id!=myshake['len_id']):
            return "Error0"
        offset +=1
        #protocol_id
        protocol_id = struct.unpack_from("19s",buf,offset)[0]
        offset += 19
        if(protocol_id!=myshake['protocol_id']):
            return "Error1"
        #reserved    
        reserved = struct.unpack_from("q",buf,offset)[0]
        offset += 8
        if(reserved!=int(myshake['reserved'])):
            return "Error2"
        #info_hash
        info_hash = struct.unpack_from("20s",buf,offset)[0]
        offset += 20
        if(info_hash!=myshake['info_hash']):
            return "Error3"
        #peer_id
        servpeer_id  = struct.unpack_from("20s",buf,offset)[0]
        offset +=20 
        if offset >= len(buf):
            raise RuntimeError("Error while reading peer port")
        return 1


def send_recv_handshake(handshake, host, port):
    """ Sends a handshake, returns the data we get back. """
    x = ""
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.send(handshake)
    try:
        s.settimeout(10)
        while (1==1):
            data = s.recv(1024)
            if(not data):
                break
            x +=data
    except:
        pass
    s.close()

    return x

hashr='f2ee74b49f25eaf3e6ab609c46eed1ef775b2c7a'.decode('hex')
params = {'info_hash' : hashr,
          'peer_id' : '-PC0001-' + ''.join([str(random.randint(0, 9)) for _ in range(12)]),
          'port' : '8090',
          'uploaded' : '0',
          'downloaded' : '0',
          'left' : '0',
          'compact': 1
          }

h,x = generate_handshake(hashr,params['peer_id'])
b = send_recv_handshake(h,'87.98.234.72',19800)
c = parsehandshake(b,x)
print(c)
#c = bencode.bdecode(b)
#print(c.keys())



